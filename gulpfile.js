const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const browserSync = require("browser-sync").create();
const minCss = require("gulp-clean-css");
const imageMin = require("gulp-imagemin");
const clean = require("gulp-clean");
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const rename = require("gulp-rename");
const uglify = require("gulp-uglify");

function html() {
    return gulp.src("./src/index.html")
    .pipe(gulp.dest("./"))
}

function css(){
    return gulp.src("./src/scss/*.scss")
    .pipe(sass()) 
    .pipe(autoprefixer())
    .pipe(minCss())
    .pipe(rename({
        suffix: ".min"
    }))
    .pipe(gulp.dest("./dist"))
}

function js() {
    return gulp.src("./src/**/*.js")
    .pipe(concat('scripts.js'))
    .pipe(rename({
        suffix: ".min"
    }))
    .pipe(uglify())
    .pipe(gulp.dest("./dist"))
}

function img(){
    return gulp.src("./src/img/**/*.*")
		.pipe(imageMin())
		.pipe(gulp.dest('./dist/img'))
}

function cleanDist() {
    return gulp.src("./dist", {allowEmpty: true}) // {read: false} 
        .pipe(clean());
}

function watch(){
    browserSync.init({
        server: {
            baseDir: "./"
        },
        port: 3000,
        notify: false,
    })
    gulp.watch("src/**/*.*", gulp.parallel(html, css, js, img)).on('change', browserSync.reload)  
}

exports.watch = gulp.series(cleanDist, html, css, img, js, watch); // 'series' виконання тасків поетапно 